<?php


namespace middlewares;


class APICsrfVerifier extends \Pecee\Http\Middleware\BaseCsrfVerifier
{
    protected $except = ['/api/*'];
}