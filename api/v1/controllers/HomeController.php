<?php


namespace api\v1\controllers;

use models\Cities;
use models\Orders;
use Rakit\Validation\Validator;

class HomeController extends \core\Controller
{
    /**
     * Список городов для api
     * @return string
     */
    public function index()
    {
        //header('HTTP/1.0 400 Bad Request');
        return Cities::all()->toJson();
    }

    /**
     * Добавление города через api
     * @return false|string
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public function send()
    {
        $opts = array(
            'title' => FILTER_SANITIZE_STRING,
            'city' => FILTER_SANITIZE_STRING,
            'message' => FILTER_SANITIZE_STRING,
            'mail'=>FILTER_VALIDATE_EMAIL,
        );
        $post = filter_input_array(INPUT_POST, $opts);
        var_dump($post);
        $validator = new Validator([
            'required' => 'Поле :attribute обязательно',
            'min' => 'В поле :attribute не может быть меньше :min символов',
            'mail' => 'В поле :attribute не валидный email'
        ]);
        $validation = $validator->validate($post, [
            'title' => 'required',
            'city' => 'required',
            'message' => 'required|min:3',
            'mail' => 'required|email',
        ]);
        if ($validation->fails()) {
            $errors = $validation->errors()->all();
            $this->getFlash('error', $errors, null, true);
            header('HTTP/1.0 400 Bad Request');
            return json_encode($errors);
        } else {
            $message =
            $city = request()->getInputHandler()->post('message')->getValue();
            $order = new Orders();
            $order->title = request()->getInputHandler()->post('title')->getValue();
            $order->order_text = request()->getInputHandler()->post('message')->getValue();
            $order->mail = request()->getInputHandler()->post('mail')->getValue();
            $order->cities_id = request()->getInputHandler()->post('city')->getValue();
            if($order->save()) {
                $mail = $this->mail('aaa@bbb.ru', $order->mail, config('fellow_worker'),'Новая заявка ID '.$order->id, $order->order_text);
                if (!$mail->send()) {
                    return $order->toJson();
                } else {
                    header('HTTP/1.0 400 Bad Request');
                    return $order->toJson();
                }
                return $order->toJson();
            }else{
                return json_encode(['error'=>'Ошибка сохранения']);
            }

        }
    }
}