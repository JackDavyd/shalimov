<?php

namespace controllers;

use Illuminate\Support\Facades\Request;
use models\Cities;
use Analogue\ORM\Analogue;
use models\Orders;
use Rakit\Validation\Validator;

class CityController extends \core\Controller
{

    /**
     * Список городов для web
     */
    public function index()
    {

        return $this->render(__CLASS__, 'list', [
            'cities'=>Cities::all(),
        ]);
    }

    /**
     * Обновление города
     * @param integer $id
     * @throws \Exception
     */
    public function update($id = null)
    {
        if (request()->getMethod() == 'post') {
            $valid = array(
                'id' => FILTER_VALIDATE_INT,
                'name' => FILTER_SANITIZE_STRING,
            );
            $post = filter_input_array(INPUT_POST, $valid);
            $validator = new Validator([
                'required' => 'Поле :attribute обязательно',
                'min' => 'В поле :attribute не может быть меньше :min символов',
            ]);
            $validation = $validator->validate($post, [
                'id'=>'required|numeric',
                'name' => 'required|min:2',
            ]);
            if ($validation->fails()) {
                $errors = $validation->errors()->all();
                $this->getFlash('error', $errors);
                redirect('/city');
            }else {
                $id = request()->getInputHandler()->post('id')->getValue();
                $city = Cities::find($id);
                $city->name = request()->getInputHandler()->post('name')->getValue();
                if ($city->save()) {
                    $this->getFlash('success', ['Город ' . $city->title . ' сохранен']);

                    redirect('/city');
                } else {
                    $this->getFlash('error', ['Ошибка сохранения города']);
                    redirect('/city');
                }
            }
        }

        if (!$id) {
            $this->getFlash('error', ['Неизвестен ID города']);
            redirect('/city');
        }

        $this->render(__CLASS__, 'update', [
            'city' => Cities::find($id),
        ]);
    }


    /**
     * Добавление города
     * @throws \Exception
     */
    public function add()
    {
        if (request()->getMethod() == 'get') {
            return $this->render(__CLASS__, 'update');
        }
        $opts = array(
            'name' => FILTER_SANITIZE_STRING,
        );
        $post = filter_input_array(INPUT_POST, $opts);
        $flash = new \Dtkahl\FlashMessages\FlashMessages;
        $validator = new Validator([
            'required' => 'Поле :attribute обязательно',
            'min' => 'Поле :attribute не пожет содержать меньше :min символов',
        ]);
        $validation = $validator->validate($post, [
            'name' => 'required|min:2',
        ]);
        if ($validation->fails()) {
            $errors = $validation->errors()->all();
            $this->getFlash('error', $errors);
            redirect('/');
        } else {
            $city = new Cities();
            $city->name = request()->getInputHandler()->post('name')->getValue();
            if($city->save()) {
                $this->getFlash('success', ['Город ' . $city->name . ' сохранен']);
                redirect('/city');
            }else{
                $this->getFlash('error', ['Ошибка сохранения города']);
                redirect('/');
            }

        }
        return $this->render(__CLASS__, 'index', [
            'cities' => Cities::all()
        ]);
    }

    /**
     * Удаление города
     * @throws \Exception
     */
    public function delete(){
        $id = request()->getInputHandler()->post('id')->getValue();
        //var_dump(Cities::destroy($id);
        $city = Cities::find($id);
        if(Cities::destroy($id)){
            $this->getFlash('success', ['Город ' . $city->name . ' удален']);
        }else{
            $this->getFlash('error', ['Ошибка удаления']);
        }

        redirect('/city');
    }
}