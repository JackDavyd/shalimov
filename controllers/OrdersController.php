<?php


namespace controllers;

use models\Cities;
use models\Orders;
use Rakit\Validation\Validator;

class OrdersController extends \core\Controller
{
    /**
     * Список заказов для web
     */
    public function index()
    {
        $this->render(__CLASS__, 'index', ['orders' => Orders::with('city')->get()]);
    }

    /**
     * Изменение заказа
     * @param null $id
     */
    public function update($id = null)
    {
        if (request()->getMethod() == 'post') {
            $valid = array(
                'id' => FILTER_VALIDATE_INT,
                'title' => FILTER_SANITIZE_STRING,
                'city' => FILTER_SANITIZE_STRING,
                'message' => FILTER_SANITIZE_STRING,
                'mail' => FILTER_VALIDATE_EMAIL,
            );
            $post = filter_input_array(INPUT_POST, $valid);

            $validator = new Validator([
                'required' => 'Поле :attribute обязательно',
                'min' => 'В поле :attribute не может быть меньше :min символов',
                'mail' => 'В поле :attribute не валидный email'
                // etc
            ]);
            $validation = $validator->validate($post, [
                'id' => 'required|numeric',
                'title' => 'required',
                'city' => 'required',
                'message' => 'required|min:3',
                'mail' => 'required|email',
            ]);
            if ($validation->fails()) {
                $errors = $validation->errors()->all();
                $this->getFlash('error', $errors);
                redirect('/orders');
            } else {
                $id = request()->getInputHandler()->post('id')->getValue();
                $order = Orders::find($id);
                $order->title = request()->getInputHandler()->post('title')->getValue();
                $order->order_text = request()->getInputHandler()->post('message')->getValue();
                $order->mail = request()->getInputHandler()->post('mail')->getValue();
                $order->cities_id = request()->getInputHandler()->post('city')->getValue();
                if ($order->save()) {
                    $this->getFlash('success', ['Заявка ' . $order->title . ' сохранена']);
                    redirect('/orders');
                } else {
                    $this->getFlash('error', ['Ошибка сохранения заявки']);
                    redirect('/orders');
                }
            }
        }

        if (!$id) {
            $this->getFlash()('error', ['Неизвестен ID заказа']);
            redirect('/orders');
        }

        $this->render(__CLASS__, 'update', [
            'order' => $order = Orders::findFirstOrFail($id),
            'cities' => Cities::all()
        ]);
    }

    /**
     * Удаление заказа
     * @throws \Exception
     */
    public function delete()
    {
        $id = request()->getInputHandler()->post('id')->getValue();
        $order = Orders::find($id);
        if(Orders::destroy($id)){
            $this->getFlash('success', ['Заявка ' . $order->title . ' удалена']);
        }else{
            $this->getFlash('success', ['Ошибка удаления заявки ' . $order->title]);
        }
        redirect('/orders');
    }
}