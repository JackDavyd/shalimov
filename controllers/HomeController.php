<?php


namespace controllers;

use models\Cities;
use models\Orders;
use Rakit\Validation\Validator;

class HomeController extends \core\Controller
{
    /**
     * Главная страница с формой
     */
    public function index()
    {
        return $this->render(__CLASS__, 'index', ['cities' => Cities::all()]);
    }

    /**
     * Добавление заказа
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public function send()
    {
        $opts = array(
            'title' => FILTER_SANITIZE_STRING,
            'city' => FILTER_SANITIZE_STRING,
            'message' => FILTER_SANITIZE_STRING,
            'mail'=>FILTER_VALIDATE_EMAIL,
        );
        $post = filter_input_array(INPUT_POST, $opts);
        $flash = new \Dtkahl\FlashMessages\FlashMessages;
        $validator = new Validator([
            'required' => 'Поле :attribute обязательно',
            'min' => 'В поле :attribute не может быть меньше :min символов',
            'mail' => 'В поле :attribute не валидный email'
            // etc
        ]);
        $validation = $validator->validate($post, [
            'title' => 'required',
            'city' => 'required',
            'message' => 'required|min:3',
            'mail' => 'required|email',
        ]);
        if ($validation->fails()) {
            $errors = $validation->errors()->all();
            $this->getFlash('error', $errors, null, true);
            redirect('/');
        } else {
            $message =
            $city = request()->getInputHandler()->post('message')->getValue();
            $order = new Orders();
            $order->title = request()->getInputHandler()->post('title')->getValue();
            $order->order_text = request()->getInputHandler()->post('message')->getValue();
            $order->mail = request()->getInputHandler()->post('mail')->getValue();
            $order->cities_id = request()->getInputHandler()->post('city')->getValue();

            if($order->save()) {
                $flash->setSuccess('success', 'Заявка ' . $order->title . ' отправлена');
                $mail = $this->mail('aaa@bbb.ru', $order->mail, config('fellow_worker'),'Новая заявка ID '.$order->id, $order->order_text);
                if (!$mail->send()) {
                    $this->getFlash('success', ['Заявка ' . $order->title . ' отправлена клиенту']);
                } else {
                    $this->getFlash('error', 'Ошибка отправки заявки на почту ' . $mail->ErrorInfo);
                }
                redirect('/orders');
            }else{
                $this->getFlash('error', ['Ошибка сохранения заявки']);
                redirect('/');
            }

        }
    }
}