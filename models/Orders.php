<?php


namespace models;
/**
 * Класс модели для таблицы orders
 */
class Orders extends \core\Model
{
    //Таблица
    protected $table = 'orders';
    //Отключение обязательных полей
    public $timestamps = false;

    /**
     * Связь с таблицей cities
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function city()
    {
        return $this->hasOne('models\Cities','id', 'cities_id');
    }
}