<?php

namespace models;

/**
 * Класс модели для таблицы cities
 */
class Cities extends \core\Model {
    //Таблица
    protected $table = 'cities';
    //отключили поля по умолчанию
    public $timestamps = false;

    /**
     * Связь с таблицей orders
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('models\Orders','id', 'cities_id');
    }
}
