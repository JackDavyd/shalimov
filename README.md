
Установка
-------------------
Предполагается что git и composer установлены глобально.

Выполнить в каталоге DOCUMENT_ROOT
````
git clone https://JackDavyd@bitbucket.org/JackDavyd/shalimov.git .
````
````
composer install
````
Дамп БД в каталоге /data. Настроить подключение в конфиге /config/main.php.


Пояснения
------------
RESTFul api чисто демонстрационное. 
Тестов нет.
Веб интерфейс полностью.

MVC структура свой велосипед.

Библиотеки

Роутер https://github.com/skipperbent/simple-php-router#multiple-http-verbs

Системные сообщения https://github.com/dantodev/php-flash-messages

ORM от Laravel, но только он. Самого фремворка нет.

Валидаторы https://github.com/rakit/validation, в orm-е есть свои, но решил пусть отдельно.

Используются в контроллерах, а не в моделях это в рамках самокритики.

Ох и трудная это работа(с) собирать фреймворк из кусков.