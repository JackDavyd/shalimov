<?php
//Роутер
use Pecee\SimpleRouter\SimpleRouter;
use Pecee\Http\Middleware\CsrfVerifier;
use middlewares\APICsrfVerifier;
SimpleRouter::group(['namespace' => 'controllers'], function () {
    SimpleRouter::csrfVerifier(new Pecee\Http\Middleware\BaseCsrfVerifier());
    SimpleRouter::get('/', 'HomeController@index');
    SimpleRouter::get('/city', 'CityController@index');
    SimpleRouter::get('/update', 'CityController@update');
    SimpleRouter::post('/send', 'HomeController@send');
    SimpleRouter::get('/orders', 'OrdersController@index');
    SimpleRouter::get('/orders/update/{id}', 'OrdersController@update');
    SimpleRouter::post('/orders/update/', 'OrdersController@update');
    SimpleRouter::post('/orders/delete/', 'OrdersController@delete');
    SimpleRouter::get('/city/update/{id}', 'CityController@update');
    SimpleRouter::post('/city/update/', 'CityController@update');
    SimpleRouter::post('/city/delete/', 'CityController@delete');
    SimpleRouter::match(['get', 'post'], '/city/add/', 'CityController@add');
});
SimpleRouter::group(['namespace' => 'api\v1\controllers'], function () {
    SimpleRouter::partialGroup('/api/v1/', function () {
        SimpleRouter::csrfVerifier(new APICsrfVerifier());
        var_dump($_POST);
        SimpleRouter::get('/', 'HomeController@index');
        //SimpleRouter::get('/city', 'CityController@index');
        //SimpleRouter::get('/update', 'CityController@update');
        SimpleRouter::post('/send', 'HomeController@send');
        /*SimpleRouter::get('/orders', 'OrdersController@index');
        SimpleRouter::get('/orders/update/{id}', 'OrdersController@update');
        SimpleRouter::post('/orders/update/', 'OrdersController@update');
        SimpleRouter::post('/orders/delete/', 'OrdersController@delete');
        SimpleRouter::get('/city/update/{id}', 'CityController@update');
        SimpleRouter::post('/city/update/', 'CityController@update');
        SimpleRouter::post('/city/delete/', 'CityController@delete');
        SimpleRouter::match(['get', 'post'], '/city/add/', 'CityController@add');*/
    });
});
