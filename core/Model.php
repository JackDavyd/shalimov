<?php

namespace core;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
/**
 * Base Class for models working database and for working with tree Adjacency List
 * Базовый класс для моделей которые работают с ORM Eloquent
 */
class Model extends \Illuminate\Database\Eloquent\Model {

    /**
     * Инициализация соединения с БД и ORM
     * Model constructor.
     */
    public function __construct() {
        //connect database
        //соединение с БД
        //реквизиты подключения в файле /config/main.php
        $config = config('db');
        $capsule = new Capsule;
        $capsule->addConnection($config);
        $capsule->setEventDispatcher(new Dispatcher(new Container));
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
        return parent::__construct();
    }

}
