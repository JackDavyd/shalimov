<?php

namespace core;

use \Exception;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

/**
 * Base Controller parent for other controllers.
 */
class Controller
{

    /**
     * Create model
     * Создаем обьект модели
     * @return model class
     * @return класс модели
     * @property string model name
     * @property строка имя модели
     */
    public function model($name)
    {
        $ns = '\models\\' . $name;
        return new $ns;
    }

    /**
     * Render views
     * Рендерим представления
     * @property string controller class (строка класс контроллера)
     * @property string view name (строка имя файла представления без .php )
     * @property array data for view (данные в представление)
     */
    public function render($controller, $view, $data = [])
    {
        //var_dump($controller); die();
        $class = (new \ReflectionClass($controller))->getShortName();
        $class = strtolower(preg_replace('/Controller/', '', $class));
        //расставляем значения массива данных в переменные
        extract($data);
        //в буфер
        ob_start();
        //файл представления (каталог определяется по имени контроллера, а имя файла по $view)
        require(__DIR__ . DIRECTORY_SEPARATOR . '../views' . DIRECTORY_SEPARATOR . $class . DIRECTORY_SEPARATOR . $view . '.php');
        //из буфера в макет
        $content = ob_get_clean();
        //грузим файл макета (можно переделать чтобы менять файлы макетов для фронтенда и для админки например)
        require(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'layout.php');
    }

    /**
     * Системные сообщения
     * @param string $key
     * @param array $values
     * @throws Exception
     */
    public function getFlash(string $key, array $values)
    {
        $flash = new \Dtkahl\FlashMessages\FlashMessages;
        switch ($key) {
            case 'success':
                $flash->setSuccess($key, $values);
                break;
            case 'error':
                $flash->setError($key, $values);
                break;
            default:
                throw new Exception('Ключ ' . $key . ' не поддерживается');
        }
    }

    /**
     * Отправка почты
     * @param $from
     * @param $replyTo
     * @param $addAddsess
     * @param $subject
     * @param $body
     * @return PHPMailer
     * @throws \PHPMailer\PHPMailer\Exception
     */
    protected function mail($from, $replyTo, $addAddsess, $subject, $body)
    {
        $mail = new PHPMailer(true);
        //@TODO вынести доступы smtp в конфиг
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;
        $mail->isSMTP();
        $mail->Host       = 'smtp.gmail.com';
        $mail->SMTPAuth   = true;
        $mail->Username   = 'aaa@gmail.com';
        $mail->Password   = 'gfhjkm';
        $mail->SMTPSecure = 'tls';
        $mail->Port       = 587;

        $mail->setFrom($from, '');
        $mail->addReplyTo($replyTo, '');
        $mail->addAddress($addAddsess, '');
        $mail->Subject = $subject;
        $mail->Body = $body;
        return $mail;

    }

}
