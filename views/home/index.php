<h4>Форма</h4>
<form action="/send" method="post">
    <div class="form-group">
        <label for="exampleFormControlInput1">Заголовок заказа</label>
        <input type="text" name="title" class="form-control" id="exampleFormControlInput1" placeholder="Введите заголовок" required>
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput2">Email</label>
        <input type="email" name="mail" class="form-control" id="exampleFormControlInput2" placeholder="name@example.com" required>
    </div>
    <div class="form-group">
        <label for="exampleFormControlSelect1">Выбрать город</label>
        <select name="city" class="form-control" id="exampleFormControlSelect1" required>
            <option value="">Выбрать</option>
            <?php foreach ($cities as $city) : ?>
                <option value="<?=$city->id?>"><?=$city->name?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Заявка</label>
        <textarea name="message" class="form-control" id="exampleFormControlTextarea1" rows="5" required></textarea>
    </div>
    <input type="hidden" name="csrf_token" value="<?= csrf_token(); ?>">
    <button type="submit" class="btn btn-primary">Отправить</button>
</form>