<h4>Форма</h4>
<form action="<?=!empty($city->id) ? '/city/update/' : '/city/add/'?>" method="post">
    <div class="form-group">
        <label for="exampleFormControlInput1">Название</label>
        <input type="text" name="name" class="form-control" id="exampleFormControlInput1" placeholder="Введите заголовок" value="<?=!empty($city->name) ? $city->name : ''?>" required>
    </div>
    <input type="hidden" name="id" value="<?=!empty($city->id) ? $city->id : 0;?>">
    <input type="hidden" name="csrf_token" value="<?= csrf_token(); ?>">
    <button type="submit" class="btn btn-primary">Обновить</button>
</form>
