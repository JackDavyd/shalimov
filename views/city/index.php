<h4>Форма</h4>
<form action="/send" method="post">
    <div class="form-group">
        <label for="exampleFormControlSelect1">Выбрать город</label>
        <select name="city" class="form-control" id="exampleFormControlSelect1">
            <option>Выбрать</option>
            <?php foreach ($cities as $city) : ?>
                <option value="<?=$city['name']?>"><?=$city['name']?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Заявка</label>
        <textarea name="message" class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
    </div>
    <input type="hidden" name="csrf_token" value="<?= csrf_token(); ?>">
    <button type="submit" class="btn btn-primary">Отправить</button>
</form>