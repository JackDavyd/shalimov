<h4>Список городов</h4>
<a href="/city/add" class="btn btn-success">Добавить</a>
<table class="table">
    <thead>
    <tr>
        <th scope="col">id</th>
        <th scope="col">Название</th>
        <th scope="col">Действия</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($cities as $city) : ?>
        <tr>
            <th scope="row"><?= $city->id ?></th>
            <td><?= $city->name ?></td>
            <td><a href="/city/update/<?= $city->id ?>" class="btn"  style="float: left" title="Изменить"><i class="fa fa-pencil"></i></a>
                <form action="/city/delete/" method="post">
                    <input type="hidden" name="id" value="<?= $city->id; ?>">
                    <input type="hidden" name="csrf_token" value="<?= csrf_token(); ?>">
                    <button type="submit" class="btn btn-link" title="Удалить"><i class="fa fa-trash-o"></i></button>

                </form>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>