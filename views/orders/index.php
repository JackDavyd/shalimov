<h4>Заказы</h4>
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">Заголовок</th>
            <th scope="col">Текст</th>
            <th scope="col">Email</th>
            <th scope="col">Город</th>
            <th scope="col">Действия</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($orders as $order) : ?>
            <tr>
                <th scope="row"><?= $order->id ?></th>
                <td><?= $order->title ?></td>
                <td><?= $order->order_text ?></td>
                <td><?= $order->mail ?></td>
                <td><?= $order->city->name ?></td>
                <td><a href="/orders/update/<?= $order->id ?>" class="btn"  style="float: left" title="Изменить"><i class="fa fa-pencil"></i></a>
                <form action="/orders/delete/" method="post">
                    <input type="hidden" name="id" value="<?= $order->id; ?>">
                    <input type="hidden" name="csrf_token" value="<?= csrf_token(); ?>">
                    <button type="submit" class="btn btn-link" title="Удалить"><i class="fa fa-trash-o"></i></button>

                </form>
                </td>
                <td>

                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>