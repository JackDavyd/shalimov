<h4>Форма</h4>
<form action="/orders/update/" method="post">
    <div class="form-group">
        <label for="exampleFormControlInput1">Заголовок заказа</label>
        <input type="text" name="title" class="form-control" id="exampleFormControlInput1" placeholder="Введите заголовок" value="<?=$order->title?>" required>
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput2">Email</label>
        <input type="email" name="mail" class="form-control" id="exampleFormControlInput2" placeholder="name@example.com" value="<?=$order->mail?>" required>
    </div>
    <div class="form-group">
        <label for="exampleFormControlSelect1">Выбрать город</label>
        <select name="city" class="form-control" id="exampleFormControlSelect1" required>
            <option value="">Выбрать</option>
            <?php foreach ($cities as $city) : ?>
            <?php if($order->cities_id == $city->id) : ?>
                <option value="<?=$city->id?>" selected><?=$city->name?></option>
            <?php else: ?>
                    <option value="<?=$city->id?>"><?=$city->name?></option>
            <?php endif; ?>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Заявка</label>
        <textarea name="message" class="form-control" id="exampleFormControlTextarea1" rows="5" required><?=$order->order_text?></textarea>
    </div>
    <input type="hidden" name="id" value="<?=$order->id;?>">
    <input type="hidden" name="csrf_token" value="<?= csrf_token(); ?>">
    <button type="submit" class="btn btn-primary">Обновить</button>
</form>
