<?php
require __DIR__ .'/vendor/autoload.php';
use Pecee\SimpleRouter\SimpleRouter;

/* подключение вспомогательных файлов */
require_once __DIR__ . '/helpers.php';
require_once __DIR__ . '/routes.php';
//Запуск сессии
if (!session_id()) @session_start();
/**
 * The default namespace for route-callbacks, so we don't have to specify it each time.
 * Can be overwritten by using the namespace config option on your routes.
 */
//Запуск роутера
SimpleRouter::setDefaultNamespace('\Demo\Controllers');

// Start the routing
SimpleRouter::start();